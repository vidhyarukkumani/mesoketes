package mesoketes;

import java.util.List;

/**
 * Created by ajira on 18/06/19.
 */
public class Day {
    private List<TribalAttack> tribalAttacks;

    public Day(List<TribalAttack> tribalAttacks) {
        this.tribalAttacks = tribalAttacks;
    }

    public int computeSuccessfulAttacks(City city) {
        int successfulAttacks = 0;

        for (TribalAttack attack : tribalAttacks) {
            if (city.defendWall(attack.getWallName(), attack.getAttackStrength())) {
                successfulAttacks++;
            }
        }
        return successfulAttacks;

    }
}
